# Chart Module


This repository provides a wrapper for integrating Chart.js as an iframe. It allows for dynamic chart creation and configuration through a postMessage interface. The module accepts three parameters: dataUrl, configUrl, and mode.

## Features

- Supports multiple data formats: JSON, CSV, and iCSV.
- Editable chart configuration.
- Two operational modes: view and edit.

## Parameters

The iframe accepts four parameters via postMessage():

- key: 'chartWrapper' // needs to be like this to avoid wrong events triggering
- dataUrl: URL to the data file. Supported formats are JSON, CSV, and iCSV.
- configUrl: URL to the chart configuration file in JSON format.
- mode: Determines the operational mode of the chart. Can be either view or edit.

## Usage Embedding the Iframe

To use the chart module, embed the iframe in your HTML and set up postMessage communication:

    <iframe id="chartIframe" src="path/to/chart-module.html" style="width: 100%; height: 400px;"></iframe>

### Sending Parameters

Use the postMessage API to send key(always needs to be 'chartWrapper') dataUrl, configUrl, and mode to the iframe:

    const iframe = document.getElementById('chartIframe');

    const message = {
        key: 'chartWrapper',
        dataUrl: 'https://example.com/data.json',
        configUrl: 'https://example.com/config.json',
        mode: 'view' // or 'edit'
    };

    iframe.contentWindow.postMessage(message, '*');


For a specific implementation have a look at the integration_iframe.html in this repo.

### Modes

In view mode, the chart module will:

- Fetch data from dataUrl.
- Fetch configuration from configUrl.
- Render the chart using the fetched data and configuration.

In edit mode, the chart module will:

- Fetch data from dataUrl.
- Fetch configuration from configUrl.
- Allow the user to edit the chart configuration.
- Save the updated configuration to disk to used as input for the view mode
