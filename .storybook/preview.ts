import 'bulma/css/bulma.css';

import type { Preview } from "@storybook/vue3";

const preview: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;

/*
// read more: https://storybook.js.org/recipes/vuetify#register-vuetify-in-storybook
// decorator for vuetify is necessary
export const decorators = [withVuetifyTheme];
*/
