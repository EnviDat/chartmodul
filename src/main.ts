import { createApp } from 'vue'
import App from './App.vue'

import ChartModule from '@/chartModulePlugin';
import 'bulma/css/bulma.css';

import {createRouter, createWebHashHistory} from "vue-router";
const routes: any[] = [
  // { path: '/' },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

createApp(App)
//  .use(ChartModule)
  .use(router)
  .mount('#appChartModule')
