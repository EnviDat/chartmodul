import type {App} from 'vue';
import TheChartApp from "./components/TheChartApp.vue";
import 'bulma/css/bulma.css';

const chartModulePlugin = {
  install(app: App, options?: any): void {
    app.component('TheChartApp', TheChartApp);
    app.provide('ChartModule - options', options);
    console.log('TheChartApp installed');
  }
}

export default chartModulePlugin;

export { TheChartApp }

