import type { Meta, StoryObj } from '@storybook/vue3';
import JSONEditorView from "@/components/JSONEditorView.vue";

const meta = {
  title: 'Editing / JSON Editor View',
  component: JSONEditorView,
} satisfies Meta<typeof JSONEditorView>;

/*
import data1 from '../../public/data1.json';
*/
import barChartConfig from "../../public/barChartConfig.json";
import lineChartConfig from "../../public/lineChartConfig.json";

export default meta;

type Story = StoryObj<typeof meta>;

export const NoJson: Story = {
  args: {
    jsonContent: undefined,
    textContent: undefined,
  },
};

export const WithTextContent: Story = {
  args: {
    jsonContent: undefined,
    textContent: JSON.stringify(barChartConfig),
    mode: 'text',
  },
};


export const WithJsonContent: Story = {
  args: {
    jsonContent: lineChartConfig,
    textContent: undefined,
  },
};

export const Readonly: Story = {
  args: {
    ...WithJsonContent.args,
    readOnly: true,
  },
};

export const WithError: Story = {
  args: {
    jsonContent: undefined,
    textContent: '{ rubish: bla  }',
    mode: 'text',
  },
};
