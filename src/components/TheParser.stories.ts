import type { Meta, StoryObj } from '@storybook/vue3';
import TheParser from "@/components/TheParser.vue";

const meta = {
  title: 'Viewer / CSV-JSON-Parser',
  component: TheParser,
} satisfies Meta<typeof TheParser>;


export default meta;

type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
  },
};

export const WithCSVClassic: Story = {
  args: {
    url: '/12-NASA-E_small_classic.csv',
  },
};

export const WithCSVNead: Story = {
  args: {
    url: '/12-NASA-E_small_nead.csv',
  },
};

/*
export const WithJSON: Story = {
  args: {
    url: '/data1.json',
  },
};
*/
