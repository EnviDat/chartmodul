import type { Meta, StoryObj } from '@storybook/vue3';
import TheChartApp from "@/components/TheChartApp.vue";
import {getMetaData} from "@/middelware/DataConversion";

import data1 from '../../public/data1.json';
import lineConfig from '../../public/lineChartConfig.json';


const meta = {
  title: 'Editor & Viewer / The Chart App',
  component: TheChartApp,
} satisfies Meta<typeof TheChartApp>;


export default meta;

type Story = StoryObj<typeof meta>;

export const NoData: Story = {
  args: {
    dto: {
      editorProps: {
        chartType: "line",
      },
      data: undefined,
    },
  },
};

export const NoConfig: Story = {
  args: {
    dto: {
      editorProps: undefined,
      data: data1,
    },
  },
};

export const NoDataAndNoConfig: Story = {
  args: {
    dto: {
      editorProps: undefined,
      data: undefined,
    },
  },
};

export const LineChart: Story = {
  args: {
    dto: {
      editorProps: {
        chartType: "line",
      },
      data: data1,
    },
  },
};

export const BarChart: Story = {
  args: {
    dto: {
      editorProps: {
        chartType: "bar",
      },
      data: data1,
    },
  },
};

export const BarWithTitle: Story = {
  args: {
    dto: {
      chartConfig: {
        type: 'bar',
        options: {
          plugins: {
            title: {
              text: "Bar Chart Title",
              display: true
            },
          }
        },
      },
      editorProps: {
        chartType: "bar",
      },
      data: data1,
    },
  },
};


export const BarWithParsingAxies: Story = {
  args: {
    dto: {
      editorProps: {
        chartType: "bar",
      },
      data: data1,
    },
  },
};

export const ViewBarWithTitle: Story = {
  args: {
    mode: 'view',
    ...BarWithTitle.args,
  },
};

export const ViewConfigOnly: Story = {
  args: {
    mode: 'view',
    dto: {
      chartConfig: {
        type: "line",
        xParam: "year",
        yParam: "count",
      },
      data: data1,
    },
  },
};


export const EditLineChart: Story = {
  args: {
    mode: 'edit',
    ...LineChart.args,
  },
};

export const EditBarChart: Story = {
  args: {
    mode: 'edit',
    ...BarWithTitle.args,
  },
};

const csvUrl = '/12-NASA-E_small_classic.csv';
let response = await fetch(csvUrl);
let inputCSV = await response.text();
// import inputCSV from '@/../public/12-NASA-E_small_classic.csv';
const metaCSV = getMetaData(inputCSV, false);

export const CSVEditorLineConfig: Story = {
  args: {
    mode: 'edit',
    dto: {
      meta: metaCSV,
      chartConfig: lineConfig,
      data: metaCSV.dataJSON,
    },
  },
};


export const CSVEditorMode: Story = {
  args: {
    mode: 'edit',
    dto: {
      meta: metaCSV,
      editorProps: {
        chartTitle: 'Bar Chart from CSV',
        chartTitleVisible: true,
        chartType: 'bar',
        yParam: 'NR',
      },
      data: metaCSV.dataJSON,
    },
  },
};

/*

const iCSVUrl = '/12-NASA-E_small_nead.csv';
response = await fetch(iCSVUrl);
inputCSV = await response.text();
const metaNEAD = getMetaData(inputCSV, false);


export const ICSVEditorMode: Story = {
  args: {
    mode: 'edit',
    dto: {
      meta: metaNEAD,
      editorProps: {
        chartTitle: 'Line Chart from iCSV',
        chartTitleVisible: true,
        chartType: 'line',
      },
      data: metaNEAD.dataJSON,
    },
  },
};

const iCSVUrl2 = '/ZHAW_TOTALP.icsv';
response = await fetch(iCSVUrl2);
inputCSV = await response.text();
const metaNEAD2 = getMetaData(inputCSV, false);


export const ICSVZHAWEditorMode: Story = {
  args: {
    mode: 'edit',
    dto: {
      meta: metaNEAD2,
      editorProps: {
        chartTitle: 'Line Chart from iCSV',
        chartTitleVisible: true,
        chartType: 'line',
      },
      data: metaNEAD2.dataJSON,
    },
  },
};
*/
