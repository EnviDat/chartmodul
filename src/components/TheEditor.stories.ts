import type {Meta, StoryObj} from '@storybook/vue3';
import TheEditor from "@/components/TheEditor.vue";
import data1 from '../../public/data1.json';
import lineChartConfig from "../../public/lineChartConfig.json";
import barChartConfig from "../../public/barChartConfig.json";
import {parseCSV} from "@/middelware/DataConversion";
import {convertToChartProps} from "@/middelware/ConfigConversion";


const meta = {
  title: 'Editing / The Editor',
  component: TheEditor,
} satisfies Meta<typeof TheEditor>;


const csvUrl = '/12-NASA-E_small_classic.csv';
const response = await fetch(csvUrl);
const inputCSV = await response.text();
const metaObj = parseCSV(inputCSV);


export default meta;

type Story = StoryObj<typeof meta>;

export const NoDataAndNoConfig: Story = {
  args: {
    dto: undefined,
  },
};

export const NoData: Story = {
  args: {
    dto: {
      editorProps: {
        chartTypes: ['bar', 'line'],
        ...convertToChartProps(lineChartConfig),
      },
      // chartConfig: lineChartConfig,
      data: undefined,
    },
  },
};

export const NoConfig: Story = {
  args: {
    dto: {
      config: undefined,
      data: data1,
    },
  },
};


export const ConfigAndData: Story = {
  args: {
    dto: {
      chartConfig: lineChartConfig,
      data: metaObj.data,
    },
  },
};

export const LineChartProps: Story = {
  args: {
    dto: {
      editorProps: {
        chartTypes: ['bar', 'line'],
        chartType: lineChartConfig.type,
        chartTitle: 'Line chart title',
      },
      data: data1,
    },
  },
};

export const BarChartProps : Story = {
  args: {
    dto: {
      editorProps: {
        chartTypes: ['bar', 'line'],
        chartType: barChartConfig.type,
        chartTitle: barChartConfig?.options?.plugins?.title?.text,
      },
      data: data1,
    }
  },
};

