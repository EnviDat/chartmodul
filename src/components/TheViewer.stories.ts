import type { Meta, StoryObj } from '@storybook/vue3';
import TheViewer from "@/components/TheViewer.vue";

const meta = {
  title: 'Viewer / The Viewer',
  component: TheViewer,
} satisfies Meta<typeof TheViewer>;

import data1 from '../../public/data1.json';

export default meta;

type Story = StoryObj<typeof meta>;

export const NoData: Story = {
  args: {
    chartConfig: {
      type: "line",
    },
    data: undefined,
  },
};

export const NoConfig: Story = {
  args: {
    chartConfig: undefined,
    data: data1,
  },
};

export const NoDataAndNoConfig: Story = {
  args: {
    chartConfig: undefined,
    data: undefined,
  },
};

export const LineChart: Story = {
  args: {
    chartConfig: {
      type: "line",
    },
    data: data1,
  },
};

export const BarChart: Story = {
  args: {
    chartConfig: {
      type: "bar",
    },
    data: data1,
  },
};

export const BarWithTitle: Story = {
  args: {
    chartConfig: {
      type: "bar",
      options: {
        animation: false,
        plugins: {
          legend: {
            display: true
          },
          title: {
            text: "Bar Chart Title",
            display: true
          },
        },
      },
    },
    data: data1,
  },
};

