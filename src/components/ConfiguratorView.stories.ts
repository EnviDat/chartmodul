import type { Meta, StoryObj } from '@storybook/vue3';
import ConfiguratorView from "@/components/ConfiguratorView.vue";
import {getMetaData, parseCSV} from "@/middelware/DataConversion";
// import inputCSV from '../../public/12-NASA-E_small_classic.csv';
import inputJSON from '../../public/data1.json';

const meta = {
  title: 'Editing / ConfiguratorView',
  component: ConfiguratorView,
} satisfies Meta<typeof ConfiguratorView>;

export default meta;

type Story = StoryObj<typeof meta>;

export const NothingFilled: Story = {
  args: {
    chartType: undefined,
    chartTypes: ['bar', 'line'],
    xParam: undefined,
    yParam: undefined,
    parameters: ['param1', 'param2'],
  },
};

export const Prefilled: Story = {
  args: {
    chartTitle: 'Chart Title Prefilled',
    chartType: 'line',
    chartTypes: ['bar', 'line'],
    xParam: undefined,
    yParam: undefined,
    parameters: ['param1', 'param2'],
  },
};

const csvUrl = '/12-NASA-E_small_classic.csv';
const response = await fetch(csvUrl);
const inputCSV = await response.text();
let metaObj = getMetaData(inputCSV, false);

export const PrefilledCSVData: Story = {
  args: {
    chartTitle: 'Prefilled Parameters from CSV',
    chartType: 'line',
    chartTypes: ['bar', 'line'],
    xParam: metaObj.metaRows.fields[3],
    yParam: metaObj.metaRows.fields[4],
    parameters: metaObj.metaRows.fields,
  },
};

/*
const jsonUrl = '/data1.json';
const jsonResponse = await fetch(jsonUrl);
const inputJson = await jsonResponse.json();
*/
metaObj = getMetaData(inputJSON, true);

export const PrefilledJSONData: Story = {
  args: {
    chartTitle: 'Prefilled Parameters from JSON',
    chartType: 'bar',
    chartTypes: ['bar', 'line'],
    xParam: metaObj.metaRows.fields[0],
    yParam: metaObj.metaRows.fields[1],
    parameters: metaObj.metaRows.fields,
  },
};
